package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.service.FacadeServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class TransformationsTest {
    private Class<?> transformationsClass;


    @InjectMocks
    private Transformations transformation;

    @BeforeEach
    public void setup() throws Exception {
        transformationsClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformations");
    }

    @Test
    public void testTransformationsClassHastransformEncodeMethod() throws Exception {
        Method encode = transformationsClass.getDeclaredMethod("transformEncode", Spell.class);
        int methodModifiers = encode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTransformationsEncodeCorrectlyImplemented() {
        Spell spell = new Spell("Safira and I went to a blacksmith to forge our sword", AlphaCodex.getInstance());
        String result = transformation.transformEncode(spell);
        assertEquals("F@MFnocsZFc:cMn:=:!aNH|HM:c:vBcnCGBXGZ|HM:aADdm:M)D:", result);
    }

    @Test
    public void testTransformationsClassHastransformDecodeMethod() throws Exception {
        Method decode = transformationsClass.getDeclaredMethod("transformDecode", Spell.class);
        int methodModifiers = decode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTransformationsDecodeCorrectlyImplemented() {
        Spell spell = new Spell("F@MFnocsZFc:cMn:=:!aNH|HM:c:vBcnCGBXGZ|HM:aADdm:M)D:", RunicCodex.getInstance());
        String result = transformation.transformDecode(spell);
        assertEquals("Safira and I went to a blacksmith to forge our sword", result);
    }

    @Test
    public void testTransformationsClassHasPrivateAddAllMethod() throws Exception {
        Method add = transformationsClass.getDeclaredMethod("addAll");
        int methodModifiers = add.getModifiers();
        assertTrue(Modifier.isPrivate(methodModifiers));
    }


}
