package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Transformations {

    private List<Transformation> transformations = new ArrayList<>();

    public String transformEncode(Spell spell){
        addAll();
        for(Transformation e:transformations){
            spell = e.encode(spell);
        }
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());
        transformations.clear();
        return spell.getText();
    }

    public String transformDecode(Spell spell){
        addAll();
        Collections.reverse(transformations);
        for(Transformation e:transformations){
            spell = e.decode(spell);
        }
        spell = CodexTranslator.translate(spell,AlphaCodex.getInstance());
        transformations.clear();
        return spell.getText();
    }

    private void addAll(){
        transformations.add(new CaesarTransformation(5));
        transformations.add(new AbyssalTransformation(5));
        transformations.add(new CelestialTransformation("ab"));
    }

}
