package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository SpellbookRepository;
    @Autowired
    private WeaponRepository weaponRepository;

    private boolean isAdapt = true;

    @Override
    public List<Weapon> findAll() {
        if(isAdapt) {
            adaptToWeapon();
            isAdapt = false;
        }
        return weaponRepository.findAll();
    }
    @Override
    public void adaptToWeapon(){
        for(Bow e:bowRepository.findAll()){
            weaponRepository.save(new BowAdapter(e));
        }
        for(Spellbook e:SpellbookRepository.findAll()){
            weaponRepository.save(new SpellbookAdapter(e));
        }

    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        String log;
        Weapon weapon = weaponRepository.findByAlias(weaponName);

        if(attackType == 1){
            log = weapon.getHolderName() + " attacked with  " + weapon.getName()
                    + " (normal attack): "+ weapon.normalAttack();
        }
        else{
            log = weapon.getHolderName() + " attacked with  " + weapon.getName()
                    + " (charged attack): "+ weapon.chargedAttack();
        }
        logRepository.addLog(log);
        weaponRepository.save(weapon);

    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
