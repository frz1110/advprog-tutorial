package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation implements Transformation{

    private int key;

    public CaesarTransformation(int key) {
        this.key = key;
    }

    public CaesarTransformation() {
        this(10);
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int codexSize = codex.getCharSize();
        int selector = encode ? key : codexSize-key;
        int n = text.length();
        char[] res = new char[n];

        for (int i=0; i<text.length(); i++) {
            int newIdx = ((codex.getIndex(text.charAt(i)) + selector)%codexSize);
            res[i] = codex.getChar(newIdx);
        }
        return new Spell(new String(res), codex);
    }
}