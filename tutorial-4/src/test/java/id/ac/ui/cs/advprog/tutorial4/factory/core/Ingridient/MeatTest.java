package id.ac.ui.cs.advprog.tutorial4.factory.core.Ingridient;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MeatTest {
    private Meat beef;
    private Meat chicken;
    private Meat fish;
    private Meat pork;

    @BeforeEach
    public void setUp(){
        beef= new Beef();
        chicken = new Chicken();
        fish = new Fish();
        pork = new Pork();
    }

    @Test
    public void testGetDescription(){
        assertEquals(beef.getDescription(), "Adding Maro Beef Meat...");
        assertEquals(chicken.getDescription(), "Adding Wintervale Chicken Meat...");
        assertEquals(fish.getDescription(), "Adding Zhangyun Salmon Fish Meat...");
        assertEquals(pork.getDescription(), "Adding Tian Xu Pork Meat...");

    }

}
