package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MenuRepoTest {
    private List<Menu> list;
    private MenuRepository menuRepo;

    @BeforeEach
    public void setUp(){
        menuRepo = new MenuRepository();
    }
    @Test
    public void testGetMenus(){
        menuRepo.add(new LiyuanSoba("liyuan"));
        menuRepo.add(new MondoUdon("mondo"));
        List<Menu> mocklist = new ArrayList<>();
        mocklist.add(new LiyuanSoba("liyuan"));
        mocklist.add(new MondoUdon("mondo"));
        ReflectionTestUtils.setField(menuRepo,
                "list", mocklist);
        assertEquals(menuRepo.getMenus(),mocklist);

    }
}
