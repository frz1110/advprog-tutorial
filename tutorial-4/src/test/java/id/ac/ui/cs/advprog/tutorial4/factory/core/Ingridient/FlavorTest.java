package id.ac.ui.cs.advprog.tutorial4.factory.core.Ingridient;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FlavorTest {
    private Flavor salty;
    private Flavor spicy;
    private Flavor sweet;
    private Flavor umami;

    @BeforeEach
    public void setUp(){
        salty= new Salty();
        spicy = new Spicy();
        sweet = new Sweet();
        umami = new Umami();
    }

    @Test
    public void testGetDescription(){
        assertEquals(salty.getDescription(), "Adding a pinch of salt...");
        assertEquals(spicy.getDescription(), "Adding Liyuan Chili Powder...");
        assertEquals(sweet.getDescription(), "Adding a dash of Sweet Soy Sauce...");
        assertEquals(umami.getDescription(), "Adding WanPlus Specialty MSG flavoring...");

    }
}
