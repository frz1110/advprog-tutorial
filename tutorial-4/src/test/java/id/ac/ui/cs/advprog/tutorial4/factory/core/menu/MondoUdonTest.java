package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MondoUdonTest {
    private Menu menu;
    private Noodle noodle;
    private Flavor flavor;
    private Meat meat;
    private Topping topping;


    @BeforeEach
    public void setUp() {
        menu = new MondoUdon("mondo");
        noodle = new Udon();
        meat = new Chicken();
        flavor = new Salty();
        topping = new Cheese();

    }
    @Test
    public void testMethodReturnCorrectValue() {
        assertEquals(menu.getName(),"mondo");
        assertEquals(menu.getNoodle().getClass(),noodle.getClass());
        assertEquals(menu.getMeat().getClass(),meat.getClass());
        assertEquals(menu.getFlavor().getClass(),flavor.getClass());
        assertEquals(menu.getTopping().getClass(),topping.getClass());
        assertEquals(menu.getMeat().getClass(),meat.getClass());
        assertEquals(menu.getFlavor().getClass(),flavor.getClass());
        assertEquals(menu.getTopping().getClass(),topping.getClass());
    }
}
