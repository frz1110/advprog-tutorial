package id.ac.ui.cs.advprog.tutorial4.factory.core.Ingridient;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;


public class NoodleTest {
    private Noodle ramen;
    private Noodle soba;
    private Noodle udon;
    private Noodle shirataki;

    @BeforeEach
    public void setUp(){
        ramen = new Ramen();
        soba = new Soba();
        udon = new Udon();
        shirataki = new Shirataki();
    }

    @Test
    public void testGetDescription(){
        assertEquals(ramen.getDescription(), "Adding Inuzuma Ramen Noodles...");
        assertEquals(soba.getDescription(), "Adding Liyuan Soba Noodles...");
        assertEquals(udon.getDescription(), "Adding Mondo Udon Noodles...");
        assertEquals(shirataki.getDescription(), "Adding Snevnezha Shirataki Noodles...");

    }


}
