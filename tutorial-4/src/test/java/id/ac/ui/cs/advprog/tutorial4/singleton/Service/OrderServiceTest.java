package id.ac.ui.cs.advprog.tutorial4.singleton.Service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {


    private OrderDrink orderDrink;
    private OrderFood orderFood;

    private Class<?> OrderServiceClass;
    private OrderServiceImpl orderService;


    @BeforeEach
    public void setup() throws Exception {

        orderDrink = OrderDrink.getInstance();
        orderFood = OrderFood.getInstance();

        OrderServiceClass = Class.forName(OrderServiceImpl.class.getName());
        orderService = new OrderServiceImpl();
    }

    @Test
    public void testOrderServiceHasOrderADrinkMethod() throws Exception {
        Method order = OrderServiceClass.getDeclaredMethod("orderADrink", String.class);
        int methodModifiers = order.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderADrink() {
        orderService.orderADrink("teh");
        assertEquals(orderDrink.getDrink(),"teh");
    }

    @Test
    public void testgetDrink() {
        assertEquals(orderService.getDrink(),orderDrink);
    }

    @Test
    public void testOrderServiceHasOrderAFoodMethod() throws Exception {
        Method order = OrderServiceClass.getDeclaredMethod("orderAFood", String.class);
        int methodModifiers = order.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }
    @Test
    public void testorderAFood() {
        orderService.orderAFood("nasgor");
        assertEquals(orderFood.getFood(),"nasgor");
    }

    @Test
    public void testgetFood() {
        assertEquals(orderService.getFood(),orderFood);
    }


}
