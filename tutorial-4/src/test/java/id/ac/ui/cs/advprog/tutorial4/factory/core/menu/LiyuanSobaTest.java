package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LiyuanSobaTest {
    private Menu menu;
    private Noodle noodle;
    private Flavor flavor;
    private Meat meat;
    private Topping topping;


    @BeforeEach
    public void setUp() {
        menu = new LiyuanSoba("liyuan");
        noodle = new Soba();
        meat = new Beef();
        flavor = new Sweet();
        topping = new Mushroom();

    }
    @Test
    public void testMethodReturnCorrectValue() {
        assertEquals(menu.getName(),"liyuan");
        assertEquals(menu.getNoodle().getClass(),noodle.getClass());
        assertEquals(menu.getMeat().getClass(),meat.getClass());
        assertEquals(menu.getFlavor().getClass(),flavor.getClass());
        assertEquals(menu.getTopping().getClass(),topping.getClass());
    }
}
