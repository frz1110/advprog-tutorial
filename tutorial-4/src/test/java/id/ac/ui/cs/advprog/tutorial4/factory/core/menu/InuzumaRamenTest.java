package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InuzumaRamenTest {
    private Menu menu;
    private Noodle noodle;
    private Flavor flavor;
    private Meat meat;
    private Topping topping;


    @BeforeEach
    public void setUp() {
        menu = new InuzumaRamen("inuzuma");
        noodle = new Ramen();
        meat = new Pork();
        flavor = new Spicy();
        topping = new BoiledEgg();

    }
    @Test
    public void testMethodReturnCorrectValue() {
        assertEquals(menu.getName(),"inuzuma");
        assertEquals(menu.getNoodle().getClass(),noodle.getClass());
        assertEquals(menu.getMeat().getClass(),meat.getClass());
        assertEquals(menu.getFlavor().getClass(),flavor.getClass());
        assertEquals(menu.getTopping().getClass(),topping.getClass());
    }
}
