package id.ac.ui.cs.advprog.tutorial4.factory.core.Ingridient;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ToppingTest {
    private Topping egg;
    private Topping cheese;
    private Topping flower;
    private Topping mushroom;

    @BeforeEach
    public void setUp(){
        egg = new BoiledEgg();
        cheese = new Cheese();
        flower = new Flower();
        mushroom = new Mushroom();
    }

    @Test
    public void testGetDescription(){
        assertEquals(egg.getDescription(), "Adding Guahuan Boiled Egg Topping");
        assertEquals(cheese.getDescription(), "Adding Shredded Cheese Topping...");
        assertEquals(flower.getDescription(), "Adding Xinqin Flower Topping...");
        assertEquals(mushroom.getDescription(), "Adding Shiitake Mushroom Topping...");

    }

}
