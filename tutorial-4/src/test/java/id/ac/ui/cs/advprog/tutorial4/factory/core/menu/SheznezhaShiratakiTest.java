package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SheznezhaShiratakiTest {
    private Menu menu;
    private Noodle noodle;
    private Flavor flavor;
    private Meat meat;
    private Topping topping;


    @BeforeEach
    public void setUp() {
        menu = new SnevnezhaShirataki("shira");
        noodle = new Shirataki();
        meat = new Fish();
        flavor = new Umami();
        topping = new Flower();

    }
    @Test
    public void testMethodReturnCorrectValue() {
        assertEquals(menu.getName(),"shira");
        assertEquals(menu.getNoodle().getClass(),noodle.getClass());
        assertEquals(menu.getMeat().getClass(),meat.getClass());
        assertEquals(menu.getFlavor().getClass(),flavor.getClass());
        assertEquals(menu.getTopping().getClass(),topping.getClass());
        assertEquals(menu.getMeat().getClass(),meat.getClass());
        assertEquals(menu.getFlavor().getClass(),flavor.getClass());
        assertEquals(menu.getTopping().getClass(),topping.getClass());
    }
}
