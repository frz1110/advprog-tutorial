## Perbedaan Lazy Instantiation dan Eager Instantiation
Jika menggunakan eager instantiation, Saat aplikasi di <i>execute</i> class singleton akan di
load, saat itulah terjadi proses instansiasi. Class hanya akan di load satu kali sehingga 
object yang dibuat hanya satu dan object sudah ada meskipun method getInstance() belum dipanggil. 
Sedangkan jika menggunakan lazy instantiation object hanya akan di instansiasi ketika static method getInstance()
dipanggil atau ketika diperukan.

## Kelebihan dan Kekurangan Lazy Instantiation
### Kelebihan
Mencegah terbuatnya object yang tidak diperlukan sehingga 
tidak ada memory yang terbuang percuma.
### Kekurangan
Terdapat isu <i>multithreading</i>. Saat threadnya banyak
bisa saja lebih dari satu object terinstansiasi
## Kekurangan dan Kelebihan Eager Instantiation
### Kelebihan
Aman untuk aplikasi <i>multithreading</i>. Dapat dipastikan
object yang terbuat hanya satu meskipun terdapat banyak thread
### Kekurangan
Object yang dibuat bisa saja tidak diperlukan sehingga memory terbuang percuma.






