package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        //ToDo: Complete Me
    }
    public void update(){
        if(guild.getQuestType().equals("D") || guild.getQuestType().equals("R")) {
            this.getQuests().add(guild.getQuest());
        }

    }
    //ToDo: Complete Me
}
