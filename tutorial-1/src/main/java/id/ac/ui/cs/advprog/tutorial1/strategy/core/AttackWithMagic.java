package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //ToDo: Complete me

    public String attack(){
        return "Expecto Patronum...";
    }
    public String getType(){
        return "Attack With Magic";
    }
}
