package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me
    public String getType(){
        return "Defend With Armor";
    }
    public String defend(){
        return "Seluruh tubuh terlindungi baju zirah...";
    }
}
